#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main()
{
	DatosMemCompartida* puntero;
	int fd;
	struct stat info;
	void* tmp;
	
	fd=open("/tmp/memoria.dat", O_RDWR|O_CREAT, 0666);
	if (fd == -1)
          perror("Error al abrir archivo memoria.dat en bot.cpp, servicio open");
	if(fstat(fd,&info)==-1)
	  perror("Error al solicitar info de memoria.dat en bot.cpp, servicio fstat");
	tmp = mmap(NULL,info.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	if(tmp==(void*)-1)
	 perror("Error al proyectar en memoria en bot.cpp, servicio mmap");
	else 
	 puntero=(DatosMemCompartida*)tmp;
	 
	if(close(fd)==-1)
	   perror("Error al cerrar el fichero memoria.dat en bot.cpp, servicio close");
	//control jugador 1
	while(1)
	{
	if(puntero->esfera.centro.y < (puntero->raqueta1.y1+puntero->raqueta1.y2)/2)
	 puntero->accion = -1;
	else if(puntero->esfera.centro.y > (puntero->raqueta1.y1+puntero->raqueta1.y2)/2)
	 puntero->accion = 1;
	else puntero->accion = 0;
	if(puntero->tiempo >= 6.0)//si jugador 2 ausente 6 segs contol del bot
	 {
	   if(puntero->esfera.centro.y < (puntero->raqueta2.y1+puntero->raqueta2.y2)/2)
	     puntero->accion2 = -1;
	   else if(puntero->esfera.centro.y > (puntero->raqueta2.y1+puntero->raqueta2.y2)/2)
	     puntero->accion2 = 1;
	   else puntero->accion2 = 0;
	 }
	usleep(25000);
	}
	return 0;
}

