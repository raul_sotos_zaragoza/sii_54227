// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>
#include <signal.h>
//#include <sys/socket.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
  Init();
}

CMundo::~CMundo()
{
  if(close(fd_fifo_logger)==-1)
    perror("Error al cerrar la FIFO fd_fifo_logger en MundoServidor.cpp, servicio close");
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	
	
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[20];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	//para que los jugadores no se salgan de la pantalla
	fondo_izq.Rebota(jugador1);
	fondo_izq.Rebota(jugador2);
	fondo_dcho.Rebota(jugador1);
	fondo_dcho.Rebota(jugador2);

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=4+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.radio=0.5f;
		puntos2++;
		
		//se actualiza el logger
		char ms[56];
		sprintf(ms,"\nJugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos2);
		if(write(fd_fifo_logger, ms, sizeof(ms))==-1)
		perror("Error al escribir fd_fifo_logger en MundoServidor.cpp, servicio write");
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-4-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio=0.5f;
		puntos1++;
		
		//se actualiza el logger
		char ms[56];
		sprintf(ms,"\nJugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos1);
		if(write(fd_fifo_logger, ms, sizeof(ms))==-1)
		perror("Error al escribir fd_fifo_logger en MundoServidor.cpp, servicio write");
	}
	
	//fin del juego
	if(puntos1 == 3 || puntos2 == 3)
	{
		char ms[]= "\nFin del juego.\n";
		write(fd_fifo_logger,ms,sizeof(ms));
		//kill(getpid(), SIGUSR1);
		exit(0);
	}
	
	char datos_mov[200];//escribe en una cadena los datos de movimiento actualizados
	sprintf(datos_mov,"%f %f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,
	esfera.centro.y,esfera.radio, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
	jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	//se envian los datos de movimiento al cliente por el socket de comunicacion
	comunicacion.Send(datos_mov, sizeof(datos_mov));
}





void CMundo::RecibeComandosJugador()
{
	while (1) {
            usleep(10);
            char aux[10], key;
            comunicacion.Receive(aux,sizeof(aux));
            sscanf(aux,"%c",&key);
            switch(key)
            {
            case's':jugador1.velocidad.y=-4;break;
            case'w':jugador1.velocidad.y=4;break;
            case'l':jugador2.velocidad.y=-4;break;
            case'o':jugador2.velocidad.y=4;break;
            }
	}
}

void* hilo_comandos(void* d)
{
      CMundo* p = (CMundo*) d;
      p->RecibeComandosJugador();
}

static void terminar(int s)
{
     if(s==SIGUSR1)
      exit(0);
     else
      exit(s);
} 

void CMundo::Init()
{
//se inician los valores del fondo
  Plano p;
  //pared inferior
  p.x1=-7; p.y1=-5; p.x2=7; p.y2=-5; paredes.push_back(p);
  //superior
  p.x1=-7; p.y1=5; p.x2=7; p.y2=5; paredes.push_back(p);
  //izq
  fondo_izq.r=0; fondo_izq.x1=-7; fondo_izq.y1=-5; fondo_izq.x2=-7; fondo_izq.y2=5;
  //der
  fondo_dcho.r=0; fondo_dcho.x1=7; fondo_dcho.y1=-5; fondo_dcho.x2=7; fondo_dcho.y2=5;
//y de los jugadores
  //jugador1 a la izq
  jugador1.g=0; jugador1.x1=-6; jugador1.y1=-1; jugador1.x2=-6; jugador1.y2=1;
  //jugador2 a la dcha
  jugador2.g=0; jugador2.x1=6; jugador2.y1=-1; jugador2.x2=6; jugador2.y2=1;
	
//amado de señales 
  struct sigaction acc;
  acc.sa_handler=&terminar;
  sigaction(SIGUSR1,&acc, NULL);
  sigaction(SIGINT,&acc,NULL);
  sigaction(SIGTERM,&acc,NULL);
  sigaction(SIGPIPE,&acc,NULL);

//abrir tuberia de servidor a logger
  fd_fifo_logger = open(fifo_logger, O_WRONLY);
  if(fd_fifo_logger == -1)
    perror("Error al abrir la fd_fifo_logger en MundoServidor.cpp, servicio open");
	
//enlace de socket de conexion
  if(conexion.InitServer(ip,puerto)==-1)
    printf("Error al abrir el servidor en MundoServidor.cpp");
//socket de comunicacion
  comunicacion=conexion.Accept();
  char n_cliente[30],ms[70];
  if(comunicacion.Receive(n_cliente,sizeof(n_cliente))==-1)
    printf("Error al recibir el nombre del cliente en MundoServidor.cpp, funcion Receive");
  sprintf(ms,"\n%s se ha conectado a la partida\n",n_cliente);
  printf("%s",ms); 
	
//crear el thread para comunicacion servidor-cliente
  if(pthread_attr_init(&atrib)==-1)
    perror("Error al iniciar objeto atrib en MundoServidor.cpp, servicio pthread_attr_init");
//iniciar el thread en modo no independiente
  if(pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE)==-1)
    perror("Error al iniciar datos del thread en MundoServidor.cpp, servicio pthread_attr_setdetachstate");
  if(pthread_create(&thid1, &atrib, hilo_comandos, this)==-1)
    perror("Error al crear el thread en MundoServidor.cpp, servicio pthread_create");
}
