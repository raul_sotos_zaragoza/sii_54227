#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "stdio.h"

int main()
{
	int fd, aux, aux2, aux3;
	char ch;
	char* nombre = "/tmp/fifo_logger";
	
	aux = mkfifo(nombre,0666); //se crea una comunicacion con nombre (FIFO)

	if(aux == 0) //mkfifo devuelve 0 si exito, -1 si fracasa
	{
		fd = open(nombre, O_RDONLY); //se abre la FIFO en modo lectura
		if(fd==-1) { //si da error en open
			perror("Error al abrir la FIFO en logger.cpp, servicio open");
			return 1;
		}
		//si se abre correctamente bucle hasta que pare de leer
		while(read(fd,&ch,1) == 1)//bucle para leer los datos de la FIFO y escribirlos 
			fprintf(stdout,"%c",ch);// en la salida estandar
		
		//gestion de errores
		aux2 = close(fd);//al cerrar
		if(aux2==-1)
		 perror("Error al cerrar la FIFO en logger.cpp, servicio close");
		 
		aux3 = unlink(nombre);//al desenlazar
		if(aux3==-1)
		 perror("Error al desenlazar la FIFO en logger.cpp, servicio unlink");
	}
	else{ //si fracaso de mkfifo
		perror("Error al crear la FIFO en logger.cpp, servicio mkfifo");
	}
	return 0;
}

