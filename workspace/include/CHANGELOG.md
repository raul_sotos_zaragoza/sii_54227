CHANGELOG
Cambios realizados:

practica1 -> nuevo comentario en fichero Mundo.h

practica2 -> se ha añadido movimiento tanto a los jugadores como a la esfera rellenando sus funciones mueve (en Raqueta.cpp y Esfera.cpp); tambien se ha añadido la funcion OnSpecialKeyboardDown en tenis.cpp para implementar con las flechas del teclado el movimiento del jugador 2, incluyendo el codigo necesario en Mundo.h y Mundo.cpp; del ejercicio propuesto se ha implementado la reducción del tamaño de la esfera durante la partida, hasta que se consigue un punto que recupera su tamaño inicial; tambien se ha añadido un poder a cada jugador (cada 3 puntos) con el que paran el movimiento del rival jugador uno con la 'x' y jugador 2 con el '0'
