// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
//practica1 raul.sotos

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq, fondo_dcho;
	Raqueta jugador1, jugador2;

	int puntos1, puntos2;

	//fd de la fifo y el directorio
	int fd_fifo_logger;
	char* fifo_logger = "/tmp/fifo_logger";
	//sockets de conexion y comunicacion
	Socket conexion;
	Socket comunicacion;
	char* ip="10.0.2.15";//direccion ip local
	int puerto=6000;//puerto de comunicacion
	//variables para el thread
	pthread_t thid1;
	pthread_attr_t atrib;

	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
